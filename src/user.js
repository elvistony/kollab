module.exports = User

function User(data) {
  data = data || {}
  this.peerID = ""
  this.stage = 0
  //this.mode = 0     // 0-peer Mode    1-host-mode

  this.Name = ""
  this.UniqueID = ""
}

//Outward HandShakes
User.prototype.givehello = function (peer,unique_id) {
    peer.send(JSON.stringify({"act":"hello","payload":unique_id}))
}

User.prototype.givei_am = function (peer,nickname) {
    peer.send(JSON.stringify({"act":"i-am","payload":nickname}))
}

//Inward HandShakes
User.prototype.gethello = function (peer,data,nickname) {
    pData = JSON.parse(data)
    this.UniqueID = pData["payload"];
    this.stage = 1;
    peer.send(JSON.stringify({"act":"ack","payload":"hello"}))
    this.givei_am(peer,nickname)
    return pData["payload"];
}



User.prototype.geti_am = function (peer,data) {
    pData = JSON.parse(data)
    this.Name = pData["payload"];
    this.stage = 2;
    peer.send(JSON.stringify({"act":"ack","payload":"i-am"}))
    console.log("Connection Setup Completed!!");
    console.log(this.UniqueID);
    console.log(this.Name);
}

User.prototype.reply = function (data) {
  decoded = JSON.parse(String(data))
  from = decoded['frm']
  if(this.UniqueID!=""){
    from=this.UniqueID
  }
  if(this.Name!=""){
    from=this.Name
  }
  document.getElementById('msgs').innerHTML+="<p><b>"+from+":</b>"+decoded["msg"]+"</p>";
}

/*--------------------------------------------------------------------------
                  PHASE 1
hello       peer1 --> peer2       {"act":"hello","payload":"my unique id"}
                  -->               (hello acknowledged)
hello       peer1 <-- peer2       {"act":"hello","payload":"my unique id"}
                  <--               (hello acknowledged)

                  PHASE 2

i-am        peer1 --> peer2       {"act":"i-am","payload":"my unique id"}
                  -->               (i-am acknowledged)
i-am        peer1 <-- peer2       {"act":"i-am","payload":"my unique id"}
                  -->               (i-am acknowledged)
---------------------------------------------------------------------------*/
