//Inclusions - Imports
// import {generate} from  "randomstring"
// import {signalhub} from 'signalhub';
// import {createSwarm} from 'webrtc-swarm'

const signalhub = require('signalhub')
const Cswarm = require('webrtc-swarm')
const rs = require('randomstring')

const User = require('./user.js')
const you = new User()
const users = {}
const participants = {}
const Debug = true;

//Checking if Site is First Launch
var unique_id = getCookie('unique_id')
if(unique_id==""){
  dlog("First Launch - Generating ID")
  var new_id =  rs.generate(9);
  unique_id=new_id;
  setCookie('unique_id',new_id, 7)
  dlog("Unique ID = "+new_id)
}else{
  alert('Welcome Back '+unique_id)
  dlog("Unique ID already Exists : "+unique_id)
}

//Loading The URL Hash
var hashcode = window.location.hash
if(hashcode.length<=1){
    dlog("General Mode - Showing both Options")
    //General Mode - No hash - showing Both options
    document.getElementById('Hostee').style.display='block'
    document.getElementById('Joinee').style.display='block'
}else if(hashcode.substring(1,5)=="host"){
  var pre_session_id = getCookie('active_room_id')
  if(pre_session_id!=""){
    dlog("Host Mode - Prev Session present")
    // Host mode with Prev Session
    //send the sorry-ticket and generate a new one and send to peers
    //Check Cookies
  }else{
    dlog("Host Mode - Showing New Room Form")
    // Host mode - New Session
    document.getElementById('Hostee').style.display='block'
    //Show New Meet Creation Form
  }
}else{
  // Join Mode
  dlog("Peer Mode - Showing Peer form")
  document.getElementById('Joinee').style.display='block'
  document.getElementById('newmeetid').value = hashcode.substring(1,hashcode.length)
}

//Set Button Event Listeners
document.getElementById('genMeet').addEventListener('click', NewRoom);
document.getElementById('sendButton').addEventListener('click', sendMessage);
document.getElementById('joinRoom').addEventListener('click', JoinRoom);

// Primary Functions
function NewRoom() {
  //Generate Meeting ID 
  document.getElementById('newmeetid').value=rs.generate(9);
  //var hostSecret = CreateKey()
  subscribe2Room(document.getElementById('newmeetid').value);
  SeekPeers();
}



function JoinRoom() {
  room = document.getElementById('newmeetid').value;
  dlog(room)
  if(room.length>0){
    dlog("Joining Room: "+room )
    subscribe2Room(room);
    SeekPeers();
  }
}

function subscribe2Room(room) {
  hub = signalhub(room, ['https://rw-signal.herokuapp.com/'])
  swarm = Cswarm(hub)
}

function CreateKey() {
  var hasher =  rs.generate(13);
  return hasher
}

function sendMessage() {

  var data = document.getElementById('msg').value
  swarm.peers.forEach(function (peer) {
    peer.send(data)
  })
}

function SeekPeers() {
  swarm.on('connect', function (peer, id) {
    if (!users[id]) {
      connect(peer,id)
      dlog("Discovered Peer: "+id)
    }
  })

  swarm.on('disconnect', function (peer, id) {
    if (users[id]) {
      document.getElementById('msgs').innerHTML+="<p><i><b>"+id+":</b>Offline</i></p>";
    }
  })
}

function connect(peer,id) {
  users[id] = new User()
  document.getElementById('accepted').innerHTML+="<br>"+id+" has Joined";
  users[id].givehello(peer,unique_id)
  //users[id].Ohello(unique_id)
  peer.on('data', function (data) {
    Communicate(peer,String(data),id)
  })
}

function Communicate(peer,data,id) {
  dlog(data)
  if(users[id].stage<2){
    users[id].reply(JSON.stringify({"frm":String(id),"msg":String(data)}))
    Introduce(peer,data,id)
  }else{
    users[id].reply(JSON.stringify({"frm":String(id),"msg":String(data)}))
    //Normal Communication
  }
}


function Introduce(peer,data,id) {
  var getData = JSON.parse(data)
  var stages = ["hello","i-am"]
  if("act" in getData){
    action = getData["act"];
    if(action=="hello"){
      uid = users[id].gethello(peer,data,document.getElementById('nickname').value)
      if(uid in participants){
        users[id]=users[participants[uid]];
        dlog("Welcome Back: "+uid)
      }else{
        participants[uid]=id;
      }
    }else if (action=="i-am") {
      users[id].geti_am(peer,data)
    }else if (action=="ack") {
      users[id].stage = stages.indexOf(getData['payload'])
    }else{
      console.log("Unknown Language");
    }
  }
}
//Supporting Functions

function dlog(msg) {
  if(Debug){
    console.log(msg);
  }
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
